import React, { Component } from 'react';
import Base from './components/baseCurrency/Base';
import './index.css';
import AppBar from 'material-ui/AppBar';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Conversions from "./components/conversions/Conversions";
import coinify from 'coinify'

const currencies = [
    { value: 0, name: 'USD', code: 'US' },
    { value: 1, name: 'JPY', code: 'JP' },
    { value: 2, name: 'BGN', code: 'BG' },
    { value: 3, name: 'CZK', code: 'CZ' },
    { value: 4, name: 'DKK', code: 'DK' },
    /* { value: 5, name: 'GDP', code: '' },
     { value: 6, name: 'HUF' },
     { value: 7, name: 'PLN' },
     { value: 8, name: 'RON' },
     { value: 9, name: 'SEK' },
     { value: 10, name: 'CHF' },
     { value: 11, name: 'NOK' },
     { value: 12, name: 'HRK' },
     { value: 13, name: 'RUB' },
     { value: 14, name: 'TRY' },
     { value: 15, name: 'AUD' },
     { value: 16, name: 'BRL' },*/

];

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
           base: 0,
           rateDate: null
        };
    }

    handleBaseChange  =  (base, rateDate) => {
     this.setState({base: base, rateDate: rateDate})
    };

    render() {
        return (
            <MuiThemeProvider>
                <div>
                    <AppBar title="Currency Converter" showMenuIconButton={false}/>
                    <Base baseChange={this.handleBaseChange} currencies={currencies}/>
                    <Conversions rateDate={this.state.rateDate} base={coinify.get(currencies[this.state.base].name)} currencies={currencies}/>
                </div>

            </MuiThemeProvider>
        );
    }
}

export default App;
