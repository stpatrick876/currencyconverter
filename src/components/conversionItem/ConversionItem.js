import './ConversionItem.scss'
import {Card, CardHeader} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import CurrencySelect from '../currencySelect/CurrencySelect'
import React, { Component } from 'react';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import Divider from 'material-ui/Divider';
import WeekTrend from "../WeekTrend/WeekTrend";

const style = {
    container: {
        position: 'relative',
    },
    refresh: {
        display: 'inline-block',
        position: 'relative',
        margin: 'auto'
    },
    card: {
        height: 592,
        backgroundColor: '#009688',
        color: '#fff'
    },
    cardHead: {
        backgroundColor: 'rgb(0, 188, 212)',
        textAlign: 'center',
    },
    addIconBox: {
        width: 200,
        height: 200,
        border: '3px dashed #fff',
        display: 'inline-block',
        backgroundColor: '#00897b',
        padding: 2,
        backgroundOrigin: 'content-box'

},
    removeBtn: {
        position: 'absolute',
        top: 0,
        right: 0
    },
    flag: {
        width: 100,
        marginTop: 20,
        border: '1px solid #ccc'
    }

};


export default class ConversionItem extends Component{
    constructor(props){
        super();
        this.state = {
            currencyIdx:'',
            base: props.base
        };
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.base !== this.state.base || nextProps.rateDate !== this.state.rateDate) {
            this.setState({currencyIdx:'', base: nextProps.base, rateDate: nextProps.rateDate});
        }
    }
     removeItem = () => {
           this.props.removeItem(this.props.idx)
    };


    handleChange = (event, index, value) => {
        this.setState({
            currencyIdx: index
        });
       };

    formatCurrency = (currency) => {
        return currency.rate.toFixed(currency.decimal_digits) + currency.symbol;
    };



    getItem() {
        let item =  ('');


        if (this.props.type  === 'ADD') {
            item = (
                    <Card style={style.card} className="center-align animated fadeIn">
                        <CardHeader
                            style={style.cardHead}
                            title=" Add Conversion"
                            titleStyle={{color: '#fff'}}
                        />
                        <div style={{display:'flex', flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', height: 500}}>
                            <CurrencySelect  value={this.state.currencyIdx} currencies={this.props.currencies} handleChange={this.handleChange} />

                            <div className="add-btn" style={style.addIconBox}>
                                <i className="material-icons" style={{fontSize:120, marginTop: 45, color: '#fff'}}>attach_money</i>
                            </div>
                            <RaisedButton fullWidth={true} primary={true} label="Add" style={{ marginTop: 20, color: '#ccc'}} onClick={() => this.props.addItem(this.state.currencyIdx)} />
                        </div>
                    </Card>
            );
        }
        else {

            if(!this.props.currency) {
                item = (
                        <Card style={style.card} className="center-align animated fadeIn">
                            <CardHeader className="center-align"
                                title="loading...."
                                style={style.cardHead}
                                titleStyle={{color: '#fff'}}
                                 iconStyle={{color: '#fff'}}
                            />

                            <RefreshIndicator
                                size={50}
                                left={20}
                                top={80}
                                loadingColor="#FF9800"
                                status="loading"
                                style={style.refresh}
                            />
                        </Card>
                );
            } else  {
                item = (
                        <Card style={style.card} className="center-align animated fadeIn">
                            <CardHeader
                                style={style.cardHead}
                                title={this.props.currency.name}
                                titleStyle={{color: '#fff'}}
                                iconStyle={{color: '#fff'}}
                            >
                                <IconButton
                                    style={style.removeBtn}
                                    iconClassName="material-icons"
                                    tooltip="Ligature"
                                    onClick={this.removeItem}
                                >
                                    remove_circle_outline
                                </IconButton>
                            </CardHeader>
                            <img style={style.flag} src={this.props.country.flag} alt=""/>
                            <h4>1{this.props.base.symbol}  = {this.formatCurrency(this.props.currency)}</h4>
                            <Divider />
                            <WeekTrend rateDate={this.props.rateDate} base={this.props.base} currency={this.props.currency}/>
                        </Card>
                );
            }

        }

        return item;
    }

  render() {

      return (
          <div className=" animated slideUp" style={{width: 500, height: 600}}>
              {this.getItem()}
          </div>

  )

  }
}

