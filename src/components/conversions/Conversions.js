import './Conversions.scss';
import React, { Component } from 'react';
import ConversionItem from '../conversionItem/ConversionItem'
import axios from 'axios';
import coinify from 'coinify'
import Snackbar from 'material-ui/Snackbar';
import * as randomID  from 'random-id';
import ConversionComparison from "../ConversionComparison/ConversionComparison";

export default class Conversions extends Component{
    constructor(props){
        super(props);
        this.state = {
          base: props.base,
          rateDate: props.rateDate,
          snackBar: {
              open: false,
              message: '',
              style: {
                  backgroundColor: 'red !important'
              }
          },
          items: [{
              id: 1,
              type: 'ADD'
               }]
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.base !== this.state.base || nextProps.rateDate !== this.state.rateDate) {
            this.setState({ items: [{id: 1, type: 'ADD'}], data: [], base: nextProps.base, rateDate: nextProps.rateDate});
        }
    }

    isItemAdded(code) {
       return this.state.items.filter((item) => {
           return item.currency && (item.currency.code === code)
       }).length >= 1;
    }

     onAddItem = (idx) => {

         const items =  this.state.items.slice();
         const currencyName = this.props.currencies[idx].name;
         const currency = coinify.get(currencyName);
         const nextId = randomID(10);
         const snackBarState = this.state.snackBar;

         if(currencyName === this.props.base.code){
             snackBarState.message = 'Cannot add same symbol as base';
             snackBarState.open = true;
             this.setState({snackBar: snackBarState });
             return false;
         }

         if(this.isItemAdded(currencyName)) {
             snackBarState.message = 'currency already added';
             snackBarState.open = true;
             this.setState({snackBar: snackBarState });
             return false;
         }

         const dateToGet = this.props.rateDate !== null ? this.formatDate(this.props.rateDate) : 'latest';
         const currencyReq = axios.get(`https://api.fixer.io/${dateToGet}?base=${this.props.base.code}&symbols=${currencyName}`);
         const countryReq = axios.get(`https://restcountries.eu/rest/v2/alpha/${this.props.currencies[idx].code}`);


         items.splice(items.length - 1, 0, {
             id: nextId ,
             type: 'ACTIVE',
         });

         this.setState({ items: items });



         axios.all([currencyReq, countryReq]).then(axios.spread((currencRes, countryRes) => {
             currency.rate = currencRes.data.rates[currencyName];
             // items.shift();
             items.filter(item => item.id === nextId)[0].currency = currency;
             items.filter(item => item.id === nextId)[0].country = countryRes.data;
             this.setState({ items: items });
         }));
     };

    onRemoveItem = (key) => {
        const items =  this.state.items.slice();
        items.forEach((item, i) => {
            if (items[i].id === key) {
                items.splice(i,1);
            }
        });

        this.setState({ items: items });

    };

    formatDate(date) {
        let year = date.getFullYear();
        let month = (date.getMonth() + 1).toString().length < 2 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
        let day = date.getDate().toString().length < 2 ? '0' + date.getDate() : date.getDate();
        return `${year}-${month}-${day}`
    }



    conversionItems(items) {
        return items.map((item) => (
            <ConversionItem
                currencies={this.props.currencies}
                key={item.id}
                idx={item.id}
                addItem={this.onAddItem}
                removeItem={this.onRemoveItem}
                type={item.type}
                currency={item.currency}
                country={item.country}
                base={this.props.base}
                rateDate={this.props.rateDate}

            />
        ));
    }
    render() {
        return (
            <div>
                <ConversionComparison  items={this.state.items}/>
                <div className="conversions-container" style={{display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between'}}>
                    {this.conversionItems(this.state.items)}
                </div>
                <div>
                    <Snackbar
                        open={this.state.snackBar.open}
                        message={this.state.snackBar.message}
                        style={this.state.snackBar.style}
                        autoHideDuration={4000}
                    />
                </div>
            </div>


        );
    }
}