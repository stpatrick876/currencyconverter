import React  from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

export default (props) => {

    const menuItems = (currencies) => {
        return currencies.map((currency) => (
            <MenuItem
                key={currency.value}
                insetChildren={true}
                value={currency.value}
                primaryText={currency.name}
            />
        ));
    }
    return (
        <SelectField
            floatingLabelText="Symbol"
            value={props.value}
            onChange={props.handleChange}
        >
            {menuItems(props.currencies)}
        </SelectField>
    );
}