import React, { Component } from 'react';
import LineChart from "recharts/es6/chart/LineChart";
import CartesianGrid from "recharts/es6/cartesian/CartesianGrid";
import XAxis from "recharts/es6/cartesian/XAxis";
import YAxis from "recharts/es6/cartesian/YAxis";
import Tooltip from "recharts/es6/component/Tooltip";
import Legend from "recharts/es6/component/Legend";
import Line from "recharts/es6/cartesian/Line";

export default class WeekTrendChart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }


    render() {
        return (

        <LineChart style={{margin: 'auto'}} width={400} height={250} data={this.props.data}
                   margin={{ top: 5, right: 30, left: 0, bottom: 5 }}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="date" stroke="#bbc7d9" />
            <YAxis stroke="#bbc7d9"/>
            <Tooltip />
            <Legend />
            <Line type="monotone" dataKey="rate" stroke="#7dc7f4" strokeWidth="5" />
        </LineChart>
        );
    }
}