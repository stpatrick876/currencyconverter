    import React, { Component } from 'react';
    import BarChart from "recharts/es6/chart/BarChart";
    import XAxis from "recharts/es6/cartesian/XAxis";
    import YAxis from "recharts/es6/cartesian/YAxis";
    import CartesianGrid from "recharts/es6/cartesian/CartesianGrid";
    import Tooltip from "recharts/es6/component/Tooltip";
    import Legend from "recharts/es6/component/Legend";
    import Bar from "recharts/es6/cartesian/Bar";
    import {Card, CardHeader, CardText} from "material-ui";


    export default class ConversionComparison extends Component{
        constructor(props){
            super(props);
            this.state = {
                items: props.items,
                data: []
            };
        }
        componentWillReceiveProps(nextProps) {
            // You don't have to do this check first, but it can help prevent an unneeded render
            if (nextProps.items !== this.state.items) {
                this.buildData( nextProps.items).then(data => {
                    this.setState({ items: nextProps.items, data: data});
                });
            }
        }
        buildData(items) {
            return new Promise((resolve, reject) => {
                const data = items.filter(item => item.type === 'ACTIVE' &&  item.hasOwnProperty('currency') )
                    .map(item => {
                        return {
                            code: item.currency.code,
                            rate: item.currency.rate
                        }
                    });
                 setInterval(() => {
                     resolve(data);
                 }, 1000);
            });

        }

        render() {

            let chart = (<h5><em>Add Multiple Conversions below then a comparison chart will be available here</em></h5>);
            if(this.state.data.length >= 2) {
                chart = (
                    <BarChart style={{margin: 'auto'}} width={600} height={300} data={this.state.data}
                              margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                        <XAxis dataKey="code"/>
                        <YAxis/>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <Tooltip/>
                        <Legend />
                        <Bar dataKey="rate" fill="#7dc7f4" barSize={30} />
                    </BarChart>
                );
            }

            return (
                <Card expanded={this.state.expanded} style={{marginBottom: 20}}>
                    <CardHeader
                        style={{backgroundColor: 'rgb(0, 188, 212)', textAlign: 'center'}}
                        title="Comparison of Selected Conversions"
                        titleStyle={{color: '#fff'}}
                        iconStyle={{color: '#fff'}}
                        actAsExpander={true}
                        showExpandableButton={true}
                    />
                    <CardText style={{backgroundColor:'#00897b', color: '#fff'}} expandable={true}>
                        {chart}
                    </CardText>
                </Card>
            );
        }
    }