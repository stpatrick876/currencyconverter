import './CountryInfo.scss'
import React from 'react';
import Avatar from 'material-ui/Avatar';

const infoRowStyle = {
    fontSize: 18
};
const style = { borderRadius: 0, verticalAlign: 'middle', marginLeft: 5 };

export default (props) => {
    return (
        <div className="country-container">
            <div className="row" style={infoRowStyle}>
                <div className="col s3">
                    <label>Country: </label>
                    {props.country.name}
                    <Avatar
                        src={props.country.flag}
                        size={30}
                        style={style}
                    />
                </div>
                <div className="col s3">
                    <label>Population: </label>
                    {props.country.population}
                </div>
                <div className="col s3">
                    <label>Lang-Lat: </label> {props.country.latlng}
                </div>
                <div className="col s3">
                    <label>Capital: </label> {props.country.capital}
                </div>
            </div>
        </div>
    )
}
