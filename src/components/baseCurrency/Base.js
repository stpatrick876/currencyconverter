import React, { Component } from 'react';
import './base.scss';
import CurrencySelect from '../currencySelect/CurrencySelect'
import SelectedBaseCurrency from '../selectedBaseCurrency/SelectedBaseCurrency';
import {Card, CardHeader, CardText, DatePicker} from "material-ui";

const style = {
    card: {
        height: 300,
        backgroundColor: '#009688',
        color: '#fff !important',
        marginTop: 20

    },
    cardHead: {
        backgroundColor: 'rgb(0, 188, 212)',
        textAlign: 'center',
    }

}
export default class Base extends Component {

    constructor() {
        super();

        const minDate = new Date('1999/01/01');
        const maxDate = new Date();



        this.state = {
            base: 0,
            rateDate: null,
            minDate: minDate,
            maxDate: maxDate,
            autoOk: true,
            disableYearSelection: false,

        };


    }
    handleChange = (event, index, value) => {
        this.setState({ base: value });
        this.props.baseChange(value, this.state.rateDate);
    };

    handleDateSelection = (event, date) => {
        this.setState({ rateDate: date });
        this.props.baseChange(this.state.base, date);
    };


    render() {
        return (
            <div className="row">
                <div className="col s12 ">
                    <Card style={style.card} className="center-align animated fadeIn">
                        <CardHeader className="center-align"
                                    title="Base Currency Options"
                                    style={style.cardHead}
                                    titleStyle={{color: '#fff'}}
                                    iconStyle={{color: '#fff'}}
                        />
                        <CardText color={'#fff'}>
                            <div className="row">
                                <div className="col s6">
                                    <CurrencySelect value={this.state.base} currencies={this.props.currencies} handleChange={this.handleChange} />
                                </div>
                                <div className="col s6">
                                    <DatePicker
                                        floatingLabelText="Conversion Date"
                                        autoOk={this.state.autoOk}
                                        minDate={this.state.minDate}
                                        maxDate={this.state.maxDate}
                                        disableYearSelection={this.state.disableYearSelection}
                                        style={{color: '#fff'}}
                                        onChange={this.handleDateSelection}
                                    />
                                </div>
                            </div>
                            <SelectedBaseCurrency currency={this.props.currencies[this.state.base]} />
                        </CardText>
                        <div >
                        </div>
                    </Card>
                </div>
            </div>
        );
    }
}

