import React, { Component } from 'react';
import axios from 'axios';
import WeekTrendChart from "../WeekTrendChart/WeekTrendChart";
import * as dateArray from 'moment-array-dates';

/*const style = {

};*/

export default class WeekTrend extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            dateRange: ''
        }
    }

    componentDidMount() {
        this.buildApiUrls().then((urls) => {
         this.getApiPromises(urls).then(proms => {
             axios.all(proms).then(responses => {
                 const dateRange = `${responses[0].data.date} - ${responses[responses.length -1].data.date}`;
                 const data = responses.map(res => {
                     const date = new Date(res.data.date);
                     const month = (date.getMonth() + 1).toString().length < 2 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
                     const dayNum = (date.getDate() + 1);
                     const day = dayNum.toString().length < 2 ? '0' + dayNum : dayNum;

                    return {
                         date: `${month}-${day}`,
                         rate: +res.data.rates[this.props.currency.code]
                     }

                 });


                 this.setState({
                     data: data,
                     dateRange: dateRange
                 });
             });
         });
     })

    }

    getApiPromises(urls) {
            const promises = [];
        return new Promise((resolve, reject) => {

           urls.forEach(url => {
               promises.push(axios.get(url))
           });

            resolve(promises);
        });
    }

    buildApiUrls = () => {
        const urls = [];
        let endDateRange = this.props.rateDate !== null ? new Date(this.props.rateDate) : new Date();
        const startRange = new Date();
        startRange.setDate(endDateRange.getDate() - 7);

        return new Promise((resolve, reject) => {
          const dates =  dateArray.range(startRange, endDateRange, 'YYYY-MMM-DD', true);

             dates.filter(this.isWeekday)
                 .forEach(date => {
                    let newDate = this.formatDate(new Date(date));
                      urls.push(`https://api.fixer.io/${newDate}?base=${this.props.base.code}&symbols=${this.props.currency.code}`);
             });

            resolve(urls);
        });
    };


    isWeekday = (date) => {
        return new Date(date).getDay() !== 6 && new Date(date).getDay() !== 0;
    };

    formatDate(date) {
        let year = date.getFullYear();
        let month = (date.getMonth() + 1).toString().length < 2 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
        let day = date.getDate().toString().length < 2 ? '0' + date.getDate() : date.getDate();
        return `${year}-${month}-${day}`
    }

    render() {
        return (
                <div className="center-align">
                    <span className="card-title">Recent Trend ({this.state.dateRange})</span>
                    <div className="card-content center-align">
                        <WeekTrendChart data={this.state.data} currency={this.props.currency}/>
                    </div>
                </div>

        );
    }
}