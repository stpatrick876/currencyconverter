import './SelectedBaseCurrency.scss'
import CountryInfo from '../countryInfo/CountryInfo';
import React, { Component } from 'react';
import axios from 'axios';

export default class SelectedBaseCurrency extends Component {
    constructor(props) {
        super();
        this.state = {
            currencyCode: props.currency.code,
            country: {
                code: ''
            }
        };
    }

    componentDidMount() {
        this.getCountryData(this.props.currency.code);
    }

    componentWillReceiveProps(nextProps) {
        // You don't have to do this check first, but it can help prevent an unneeded render
        if (nextProps.currency.code !== this.state.currencyCode) {
            this.setState({ currencyCode: nextProps.currency.code });
            this.getCountryData(nextProps.currency.code);
        }
    }

    getCountryData(code) {
        axios.get(`https://restcountries.eu/rest/v2/alpha/${code}`)
            .then( (response) => {
                this.setState({
                    country: response.data
                });

            })
            .catch( (error) =>{
                console.log(error);
            });
    }

    render() {
        return (
            <div className="selected-base-currency">
                <h3 className="center-align">{this.props.currency.name}</h3>
                <CountryInfo country={this.state.country} />
            </div>
        )
    }

}